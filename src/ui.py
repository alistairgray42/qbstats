#! venv/bin/python

"""
Contains the main UI classes, made in Qt.
"""
import sys
import urllib.request
from bs4 import BeautifulSoup
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot, QModelIndex
from PyQt5.QtWidgets import QWidget, QApplication, QHBoxLayout, QTableView, \
    QAction, QLabel, QLineEdit, QDialog, QSpacerItem, QSizePolicy, \
    QProgressBar, QDialogButtonBox, QListWidget, QListWidgetItem, QHeaderView
from PyQt5.Qt import QMainWindow, QAbstractItemView, QVBoxLayout, \
    QComboBox, QPushButton, QGuiApplication, QItemSelectionModel, QThread,\
    QRunnable, QThreadPool
import import_stats
import scraper
from models import *


class MainWindow(QMainWindow):
    """
    A Qt main window for the application. Displays information about PPBs, allows
    for editing of tourney_data, and provides places to access the other dialogs.
    """
    # probably put this somewhere else; add more?
    statuses = [
        "",
        "Checked",
        "Unchecked",
        "Hidden"
    ]

    def __init__(self):
        super().__init__()
        self.item_model = TournamentListModel()
        self.info_model = TournamentInfoModel()
        self.setup_ui()

    def setup_ui(self):
        """
        Sets up the UI of the window. Currently a giant pile of code that I
        should split up sooner or later
        """
        menubar = self.menuBar()

        def import_all():
            import_window = ImportAllDialog()
            import_window.exec_()
        
        def show_sets():
            set_window = SetWindow()
            set_window.exec_()
        
        def show_teams():
            teams_window = TeamWindow()
            teams_window.exec_()
            
        options = menubar.addMenu('Options')

        action_import_all = QAction("Import All", self)
        action_import_all.triggered.connect(import_all)
        options.addAction(action_import_all)
        
        action_show_sets = QAction("Show Sets", self)
        action_show_sets.triggered.connect(show_sets)
        options.addAction(action_show_sets)
        
        action_show_teams = QAction("Show Teams", self)
        action_show_teams.triggered.connect(show_teams)
        options.addAction(action_show_teams)

        central_widget = QWidget()
        central_layout = QVBoxLayout(central_widget)

        controls_widget = QWidget()
        controls_layout = QHBoxLayout(controls_widget)
        controls_widget2 = QWidget()
        controls_layout2 = QHBoxLayout(controls_widget2)
        selection_widget = QWidget()
        selection_layout = QHBoxLayout(selection_widget)

        listview_widget = QWidget()
        leftpane_layout = QVBoxLayout(listview_widget)
        tableview_widget = QWidget()
        rightpane_layout = QVBoxLayout(tableview_widget)

        leftpane_remove_widget = QWidget()
        leftpane_remove_sublayout = QHBoxLayout(leftpane_remove_widget)

        name_label = QLabel(self)
        self.name_box = QLineEdit(self)
        date_label = QLabel(self)
        self.date_box = QLineEdit(self)
        status_label = QLabel(self)
        self.status_box = QComboBox(self)
        self.next_button = QPushButton(self)

        division_label = QLabel(self)
        self.division_box = QLineEdit(self)
        set_label = QLabel(self)
        self.set_box = QLineEdit(self)
        location_label = QLabel(self)
        self.location_box = QLineEdit(self)
        self.copy_url_button = QPushButton(self)
        source_label = QLabel(self)
        self.source_box = QLineEdit(self)

        self.remove_button1 = QPushButton(self)
        self.remove_button2 = QPushButton(self)
        self.remove_all_button = QPushButton(self)

        name_label.setText("Name")
        date_label.setText("Date")
        status_label.setText("Status")
        self.next_button.setText("Next")

        division_label.setText("Division")
        set_label.setText("Set")
        location_label.setText("Location")
        source_label.setText("Source")
        self.copy_url_button.setText("Copy URL")

        self.remove_button1.setText("Remove Tournament")
        self.remove_button2.setText("Remove Team")
        self.remove_all_button.setText("Remove All Tournaments")

        # self.division_box.setReadOnly(True)
        # self.date_box.setReadOnly(True)
        # self.location_box.setReadOnly(True)
        # self.source_box.setReadOnly(True)

        self.date_box.setMaximumWidth(85)
        self.source_box.setMaximumWidth(85)
        self.division_box.setMaximumWidth(85)
        self.set_box.setMaximumWidth(85)

        for i in self.statuses:
            self.status_box.addItem(i)

        controls_layout.addWidget(name_label)
        controls_layout.addWidget(self.name_box)
        controls_layout.addWidget(date_label)
        controls_layout.addWidget(self.date_box)
        controls_layout.addWidget(status_label)
        controls_layout.addWidget(self.status_box)
        controls_layout.addWidget(self.next_button)

        controls_layout2.addWidget(division_label)
        controls_layout2.addWidget(self.division_box)
        controls_layout2.addWidget(set_label)
        controls_layout2.addWidget(self.set_box)
        controls_layout2.addWidget(location_label)
        controls_layout2.addWidget(self.location_box)
        controls_layout2.addWidget(self.copy_url_button)
        controls_layout2.addWidget(source_label)
        controls_layout2.addWidget(self.source_box)

        def change_multiple_fields(editor):
            """
            When multiple fields are selected to be edited, semi-hackily edit them
            all to have the same value.
            """
            super(QTableView, self.right_pane).commitData(editor)
            value = self.info_model.last_changed_value
            # that variable being a semi-ugly (relatable) hack to get this to work
            for i in self.right_pane.selectionModel().selectedIndexes():
                self.info_model.setData(i, value, Qt.EditRole)

        self.right_pane = QTableView(self)
        self.right_pane.commitData = change_multiple_fields
        self.left_pane = QTableView(self)

        self.left_pane.setModel(self.item_model)
        self.right_pane.setModel(self.info_model)

        self.left_pane.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.right_pane.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.left_pane.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.right_pane.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        
        self.right_pane.setSelectionMode(QAbstractItemView.ExtendedSelection)
        
        # sets panes to be sortable
        self.left_pane.setSortingEnabled(True)
        self.right_pane.setSortingEnabled(True)
        
        # above lines sort left pane weirdly, so sort well
        self.left_pane.sortByColumn(0, Qt.AscendingOrder)
        
        self.name_box.textChanged.connect(self.edit_name)
        self.status_box.currentTextChanged.connect(self.edit_status)
        self.copy_url_button.pressed.connect(self.copy_url)

        self.remove_button1.pressed.connect(self.remove_tourney)
        self.remove_button2.pressed.connect(self.remove_team)
        self.next_button.pressed.connect(self.next_tourney)

        self.left_pane.selectionModel().selectionChanged.connect(self.change_tournament)

        leftpane_remove_sublayout.addWidget(self.remove_button1)
        leftpane_remove_sublayout.addWidget(self.remove_all_button)

        leftpane_layout.addWidget(self.left_pane)
        leftpane_layout.addWidget(leftpane_remove_widget)
        rightpane_layout.addWidget(self.right_pane)
        rightpane_layout.addWidget(self.remove_button2)

        central_layout.addWidget(controls_widget)
        central_layout.addWidget(controls_widget2)
        central_layout.addWidget(selection_widget)

        central_widget.setSizePolicy(QSizePolicy(
            QSizePolicy.Expanding,
            QSizePolicy.Expanding
        ))

        selection_layout.addWidget(listview_widget)
        selection_layout.addWidget(tableview_widget)

        central_widget.setLayout(central_layout)
       
        self.setCentralWidget(central_widget)
        self.setGeometry(50, 50, 1000, 800)
        self.setWindowTitle("QB Stats!")
        self.show()

    def edit_name(self):
        """
        Edit the name of the tournament to what's in the name_box.
        """
        self.info_model.tourney_data['name'] = self.name_box.text()

    def edit_status(self):
        """
        Edit the status of the tournament to what's in the status_box.
        """
        if self.status_box.currentText():
            self.info_model.tourney_data['status'] = self.status_box.currentText()

    def copy_url(self):
        """
        Copy the stats URL to the system clipboard.
        """
        QGuiApplication.clipboard().setText(self.url_box.text())

    def change_tournament(self):
        """
        Write all changed tourney_data to the database, and update all fields and the
        info table for a new tournament selected in the left pane.
        """
        self.info_model.write_new_data()

        selectedIndexes = self.left_pane.selectionModel().selectedIndexes()

        if not selectedIndexes:
            # if we selected a non-name column
            return

        self.item_model.update_names()
        row = selectedIndexes[0].row()
        self.info_model.init_data(*self.item_model.data[row])
        self.right_pane.clearSelection()
        self.right_pane.viewport().repaint()

        data = self.info_model.tourney_data

        self.name_box.setText(data['name'])
        self.source_box.setText(data['source'])
        self.date_box.setText(data['date'])
        self.status_box.setCurrentIndex(self.statuses.index(data['status']))

        # self.next_button.setFocus()

    def next_tourney(self):
        """
        When the "Next" button is pressed, sets the current tourney's status
        to "Checked" and moves to the next tournament in the list.
        """
        # Hard-coded index of 1 for "Checked"
        self.status_box.setCurrentIndex(1)
        self.edit_status()
        # Selects the next tournament; could probably be done more elegantly
        self.left_pane.selectionModel().setCurrentIndex(
            self.left_pane.selectionModel().currentIndex().sibling(
                self.left_pane.selectionModel().currentIndex().row() + 1, 0),
            QItemSelectionModel.ClearAndSelect)

    def remove_tourney(self):
        """
        Remove the selected tournament from the database
        """
        selected = self.left_pane.selectionModel().selectedRows(0)[0].row()
        self.item_model.removeRow(selected, QModelIndex())
        self.right_pane.update()

    def remove_team(self):
        """
        Remove the selected team for this tournament's results
        """
        selected = self.right_pane.selectionModel().selectedIndexes()[0].row()
        self.info_model.removeRow(selected, QModelIndex())
        

class AddChoicesRunner(QRunnable):
    """
    Thread subclass that populates an internal list of division names from HSQB
    stats pages
    """
        
    def __init__(self, urls):
        QThread.__init__(self)
        self.urls = urls
        self.choices = []
        self.names = []
        self.links = []
        self.index = 0
    
    def populate_one(self):
        """
        Adds a single list of choices to self.choices; takes the first one from 
        self.urls that has valid stats
        """
        for url in self.urls[self.index:]:
            content = urllib.request.urlopen(url).read()
            soup = BeautifulSoup(content, 'html.parser')
            if soup.find(id="Stats"):
                self.choices.append([s.text for s in soup.find(id="Stats").findAll("li")])
                self.links.append([s.a['href'] for s in soup.find(id="Stats").findAll("li")])
                self.names.append(soup.findAll("h2")[1].text)
                if len(self.choices) == 1:
                    self.index += 1
                    break
            self.index += 1            

    def run(self):
        """
        Adds all tournaments in self.urls that have valid stats to self.choices
        """
        for url in self.urls:
            self.populate_one()


class ImportOneRunner(QRunnable):
    """
    A thread to import one HSQB tournament at a time
    """

    def __init__(self, url, name):
        QThread.__init__(self)
        self.url = url
        self.name = name

    def run(self):
        """
        Scrapes one HSQB tournament from the given URL and writes it to the database.
        """
        data = import_stats.hsqb_one(self.url)
        data['division'] = self.name

        # db_io.write(tourney_data)


class ImportAllDialog(QDialog):
    """
    A simple dialog providing buttons to import all NAQT or HSQB stats to the Mongo
    database, as well as a progress bar (only currently for NAQT stats)
    """
    def __init__(self):
        super().__init__()
        self.setup_ui()
        self.threadpool = QThreadPool()
    
    def import_naqt(self):
        """
        Import all NAQT tournaments to the database
        """

        url = "http://www.naqt.com/stats/tournament/?year_code=2017&audience_id=1002"
        self.progress_bar.setMaximum(import_stats.naqt_count(url))
        
        completed = 0
        for stat_url in import_stats.naqt_many(url):
            scraper.naqt_parse(stat_url)
            completed += 1
            self.progress_bar.setProperty("value", completed)
    
    def import_hsqb(self):
        """
        Import all HSQB tournaments to the database
        """

        dialog = ImportUserInputDialog()
        
        statsurl = "http://www.hsquizbowl.org/db/tournaments/search/4/?level=2"
        statsurls = import_stats.hsqb_many(statsurl)
        
        choices_runner = AddChoicesRunner(statsurls)
        choices_runner.populate_one()
        self.threadpool.start(choices_runner)
        
        # This is really ugly
        current = 0

        @pyqtSlot(list, list)
        def next_selection(names=None, urls=None):
            nonlocal current
            
            if urls:
                for url in urls:
                    runner = ImportOneRunner(url, names[urls.index(url)])
                    self.threadpool.start(runner)

            if current == len(choices_runner.choices):
                dialog.close()
                return
            
            dialog.set_choices(choices_runner.choices[current])
            dialog.links = choices_runner.links[current]
            dialog.label.setText(choices_runner.names[current])
            current += 1
        
        dialog.selection_confirmed.connect(next_selection)
        next_selection()
    
        dialog.show()
        dialog.exec_()
        
    def setup_ui(self):
        """
        Set up the dialog interface
        """
        self.setGeometry(100, 100, 400, 300)
        
        vertical_layout = QVBoxLayout(self)
        
        naqt_button = QPushButton()
        hsqb_button = QPushButton()
        
        spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        label = QLabel(self)
        label.setAlignment(Qt.AlignCenter)
        
        self.progress_bar = QProgressBar(self)
        self.progress_bar.setFormat("%v/%m")
        
        button_box = QDialogButtonBox(self)
        button_box.setOrientation(Qt.Horizontal)
        button_box.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        
        vertical_layout.addWidget(naqt_button)
        vertical_layout.addWidget(hsqb_button)
        vertical_layout.addItem(spacer)
        vertical_layout.addWidget(label)
        vertical_layout.addWidget(self.progress_bar)
        vertical_layout.addWidget(button_box)

        self.setWindowTitle("Import All")
        naqt_button.setText("Import all from NAQT")
        hsqb_button.setText("Import all from HSQB")
        label.setText("Progress")

        naqt_button.pressed.connect(self.import_naqt)
        hsqb_button.pressed.connect(self.import_hsqb)

        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        
        self.setWindowTitle("Import All Tournaments")
        
        self.show()


class ImportUserInputDialog(QDialog):
    """
    A simple dialog allowing the user to select which divisions will get stats.
    """
    # Signal emitted when the current selection is confirmed, containing the list of
    # integer indices of the selected options.
    selection_confirmed = pyqtSignal(list, list)
    
    def __init__(self):
        super().__init__()
        
        self.choices = []
        self.items = []
        self.links = []
        
        self.setup_ui()
    
    def confirm_selection(self):
        """
        Emits the selection_confirmed signal containing selected divisions' names and urls
        """
        urls = []
        names = []
        for i in range(len(self.items)):
            if self.items[i].checkState() == Qt.Checked:
                urls.append("http://www.hsquizbowl.org/db/" + self.links[i])
                names.append(self.choices[i])
        self.selection_confirmed.emit(names, urls)
    
    def set_choices(self, choices):
        """
        Given a list of text strings as choices, update the displayed ListWidget
        to show those choices, with automatically detected box-checking.
        """
        self.choices = choices
        self.items = []
        for item in range(self.list_widget.count()):
            self.list_widget.takeItem(0)
        for choice in choices:
            item = QListWidgetItem(self.list_widget)
            item.setText(choice)
            # for case-insensitive comparison
            choice = choice.casefold()
            if "all games" in choice or "combined" in choice or \
               "full" in choice or "final" in choice:
                item.setCheckState(Qt.Checked)
            else:
                item.setCheckState(Qt.Unchecked)
            self.items.append(item)
        
    def setup_ui(self):
        """
        Set up the dialog interface
        """
        self.setGeometry(200, 200, 400, 300)
        
        self.layout = QVBoxLayout(self)
        self.label = QLabel(self)
        self.list_widget = QListWidget(self)     
        self.next = QPushButton()
        
        self.label.setText("Tournament")
        self.next.setText("Next")
        
        self.layout.addWidget(self.label)       
        self.layout.addWidget(self.list_widget)
        self.layout.addWidget(self.next)
        
        self.next.pressed.connect(self.confirm_selection)
        
        self.setWindowTitle("Select Imported Divisions")
        
        self.show()


class SetWindow(QDialog):
    def __init__(self):
        super().__init__()
        
        self.setGeometry(250, 250, 240, 400)
        
        self.setView = QTableView()
        self.setView.setModel(SetListModel())
        
        self.setView.setSortingEnabled(True)
        self.setView.model().sort(1, Qt.DescendingOrder)
        
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.setView)
        
        self.setWindowTitle("Sets")
        
        self.show()
        

class TeamWindow(QDialog):
    def __init__(self):
        super().__init__()
        
        self.setGeometry(300, 300, 400, 400)
        
        self.teamView = QTableView()
        self.teamView.setModel(TeamListModel())
        self.teamView.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.teamView.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        
        self.teamView.setSortingEnabled(True)
        self.teamView.model().sort(1, Qt.AscendingOrder)
        self.teamView.model().sort(2, Qt.DescendingOrder)
        
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.teamView)
        
        self.setWindowTitle("Teams")
        
        self.show()


app = QApplication(sys.argv)
window = MainWindow()
sys.exit(app.exec_())
