"""
Contains the UI classes for the "import" dialogs
"""
import urllib.request
from bs4 import BeautifulSoup
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QLabel, QDialog, QSpacerItem, QSizePolicy, \
    QProgressBar, QDialogButtonBox, QListWidget, QListWidgetItem
from PyQt5.Qt import QVBoxLayout, QPushButton, QThread, QRunnable, QThreadPool
import import_stats, db_io


class AddChoicesRunner(QRunnable):
    """
    Thread subclass that populates an internal list of division names from HSQB
    stats pages
    """

    def __init__(self, urls):
        QThread.__init__(self)
        self.urls = urls
        self.choices = []
        self.names = []
        self.links = []
        self.index = 0

    def populate_one(self):
        """
        Adds a single list of choices to self.choices; takes the first one from
        self.urls that has valid stats
        """
        for url in self.urls[self.index:]:
            content = urllib.request.urlopen(url).read()
            soup = BeautifulSoup(content, 'html.parser')
            if soup.find(id="Stats"):
                self.choices.append([s.text for s in soup.find(id="Stats").findAll("li")])
                self.links.append([s.a['href'] for s in soup.find(id="Stats").findAll("li")])
                self.names.append(soup.findAll("h2")[1].text)
                if len(self.choices) == 1:
                    self.index += 1
                    break
            self.index += 1

    def run(self):
        """
        Adds all tournaments in self.urls that have valid stats to self.choices
        """
        for url in self.urls:
            self.populate_one()


class ImportOneHSQBRunner(QRunnable):
    """
    A runner to import one HSQB tournament at a time
    """

    def __init__(self, url, name):
        QThread.__init__(self)
        self.url = url
        self.name = name

    def run(self):
        """
        Scrapes one HSQB tournament from the given URL and writes it to the database.
        """
        data = import_stats.hsqb_one(self.url)
        data['division'] = self.name

        db_io.write(data)


class ImportOneNAQTThread(QThread):
    """
    A thread to import one NAQT tournament at a time
    """

    def __init__(self, url):
        super().__init__()
        self.url = url

    def run(self):
        """
        Scrapes one NAQT tournament from the given URL and writes it to the database.
        """
        data = import_stats.naqt_one(self.url)
        if data:
            for tourney in data:
                db_io.write(tourney)


class ImportManyNAQTThread(QThread):
    """
    A thread to import many NAQT tournaments
    """

    finished_one = pyqtSignal()

    def __init__(self, urls):
        super().__init__()
        self.urls = urls
        self.sub_threads = []
        self.stop = False

    def run(self):
        """
        Creates an ImportOneNAQTThread for each url given and runs them
        """
        for url in self.urls:
            if self.stop:
                break
            self.sub_threads.append(ImportOneNAQTThread(url))
            self.sub_threads[-1].finished.connect(self.finished_one)
            self.sub_threads[-1].start()
            self.sleep(1)


class ImportAllDialog(QDialog):
    """
    A simple dialog providing buttons to import all NAQT or HSQB stats to the Mongo
    database, as well as a progress bar (only currently for NAQT stats)
    """

    finished_one = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.thread = None
        self.setup_ui()

    def abort_import(self):
        """
        Allow currently-running threads importing stats to finish, but prevent new ones being made
        """
        self.thread.stop = True

    def import_naqt(self):
        """
        Import all NAQT tournaments to the database
        """

        url = "http://www.naqt.com/stats/tournament/?year_code=2017&audience_id=1002&length=Any&state_code=&radius=&center_description="

        self.progress_bar.setMaximum(import_stats.naqt_count(url))
        self.progress_bar.setProperty("value", 0)

        @pyqtSlot()
        def increment_progress():
            self.progress_bar.setProperty("value", self.progress_bar.property("value") + 1)

        self.thread = ImportManyNAQTThread(import_stats.naqt_many(url))
        self.thread.finished_one.connect(increment_progress)
        self.thread.finished_one.connect(self.finished_one.emit)
        self.thread.start()

    def import_hsqb(self):
        """
        Import all HSQB tournaments to the database
        """

        dialog = ImportUserInputDialog()

        statsurl = "http://www.hsquizbowl.org/db/tournaments/search/?dist=&loc=&state=&level=2&name=&host=&dates=any&startdate=&enddate="
        statsurls = import_stats.hsqb_many(statsurl)

        choices_runner = AddChoicesRunner(statsurls)
        choices_runner.populate_one()
        # self.threadpool.start(choices_runner)
        self.threads.append(choices_runner)
        self.threads[-1].start()

        # This is really ugly
        current = 0

        @pyqtSlot(list, list)
        def next_selection(names=None, urls=None):
            nonlocal current

            if urls:
                for url in urls:
                    runner = ImportOneHSQBRunner(url, names[urls.index(url)])
                    self.threads.append(runner)
                    self.threads[-1].start()
                    # self.threadpool.start(runner)

            if current == len(choices_runner.choices):
                dialog.close()
                return

            dialog.set_choices(choices_runner.choices[current])
            dialog.links = choices_runner.links[current]
            dialog.label.setText(choices_runner.names[current])
            current += 1

        dialog.selection_confirmed.connect(next_selection)
        next_selection()

        dialog.show()
        dialog.exec_()

    def setup_ui(self):
        """
        Set up the dialog interface
        """
        self.setGeometry(100, 100, 400, 300)

        vertical_layout = QVBoxLayout(self)

        naqt_button = QPushButton()
        hsqb_button = QPushButton()
        abort_button = QPushButton()

        spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        label = QLabel(self)
        label.setAlignment(Qt.AlignCenter)

        self.progress_bar = QProgressBar(self)
        self.progress_bar.setFormat("%v/%m")

        button_box = QDialogButtonBox(self)
        button_box.setOrientation(Qt.Horizontal)
        button_box.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)

        vertical_layout.addWidget(naqt_button)
        vertical_layout.addWidget(hsqb_button)
        vertical_layout.addWidget(abort_button)
        vertical_layout.addItem(spacer)
        vertical_layout.addWidget(label)
        vertical_layout.addWidget(self.progress_bar)
        vertical_layout.addWidget(button_box)

        self.setWindowTitle("Import All")
        naqt_button.setText("Import all from NAQT")
        hsqb_button.setText("Import all from HSQB")
        abort_button.setText("Abort Import")
        label.setText("Progress")

        naqt_button.pressed.connect(self.import_naqt)
        hsqb_button.pressed.connect(self.import_hsqb)
        abort_button.pressed.connect(self.abort_import)

        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        self.show()


class ImportUserInputDialog(QDialog):
    """
    A simple dialog allowing the user to select which divisions will get stats.
    """
    # Signal emitted when the current selection is confirmed, containing the list of
    # integer indices of the selected options.
    selection_confirmed = pyqtSignal(list, list)

    def __init__(self):
        super().__init__()

        self.choices = []
        self.items = []
        self.links = []

        self.setup_ui()

    def confirm_selection(self):
        """
        Emits the selection_confirmed signal containing selected divisions' names and urls
        """
        urls = []
        names = []
        for i in range(len(self.items)):
            if self.items[i].checkState() == Qt.Checked:
                urls.append("http://www.hsquizbowl.org/db/" + self.links[i])
                names.append(self.choices[i])
        self.selection_confirmed.emit(names, urls)

    def set_choices(self, choices):
        """
        Given a list of text strings as choices, update the displayed ListWidget
        to show those choices, with automatically detected box-checking.
        """
        self.choices = choices
        self.items = []
        for item in range(self.list_widget.count()):
            self.list_widget.takeItem(0)
        for choice in choices:
            item = QListWidgetItem(self.list_widget)
            item.setText(choice)
            # for case-insensitive comparison
            choice = choice.casefold()
            if "all games" in choice or "combined" in choice or \
                    "full" in choice or "final" in choice:
                item.setCheckState(Qt.Checked)
            else:
                item.setCheckState(Qt.Unchecked)
            self.items.append(item)

    def setup_ui(self):
        """
        Set up the dialog interface
        """
        self.setGeometry(200, 200, 400, 300)

        self.layout = QVBoxLayout(self)
        self.label = QLabel(self)
        self.list_widget = QListWidget(self)
        self.next = QPushButton()

        self.label.setText("Tournament")
        self.next.setText("Next")

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.list_widget)
        self.layout.addWidget(self.next)

        self.next.pressed.connect(self.confirm_selection)

        self.show()
