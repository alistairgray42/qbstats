#! venv/bin/python
import datetime
import re

from PyQt5.Qt import QVariant, QAbstractTableModel, QModelIndex
from PyQt5.QtCore import Qt
import mysql.connector as mariadb

"""
Contains two implementations of models needed for the views in the main window
at ui.py. (Consider splitting into two files)
"""

connection = mariadb.connect(user='qbstats', database='qbstats')
cursor = connection.cursor()


class TournamentListModel(QAbstractTableModel):
    """
    Extends QAbstractListModel to provide a model for listing tournaments (the
    corresponding view is the left pane of the main window), simply listing
    tournament names, taken from the mongo_tourneys database in Mongo.
    """
    
    def __init__(self):
        super().__init__()
        self.data = []
        self.update_names()
    
    def update_names(self):
        """
        Refreshes the list of tournament names and divisions; helpful if the name of a
        tournament is edited.
        """
        cursor.execute("SELECT name,division FROM tourneys ORDER BY name;")
        self.data = [item for item in cursor]

    def removeRow(self, row, parent):
        """
        Removes the row with the given number
        """
        self.beginRemoveRows(QModelIndex(), len(self.names), len(self.names) - 1)

        query = "DELETE results, tourneys FROM results JOIN tourneys ON results.tourney_id = tourneys.id" \
                " WHERE tourneys.name = '{}' LIMIT 1;".format(self.names[row])
        cursor.execute(query)

        del self.data[row]
        self.endRemoveRows()

    def flags(self, index):
        if index.column() == 0:
            return Qt.ItemIsSelectable | Qt.ItemIsEnabled
        else:
            return Qt.ItemIsEnabled

    def rowCount(self, parent):
        """
        Returns the number of tournaments in the database
        """
        if self.data or not parent.isValid():
            return len(self.data)
        else:
            return 0

    def columnCount(self, parent):
        return 2

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole and orientation == Qt.PortraitOrientation:
            if section == 0:
                return "Tournament Name"
            elif section == 1:
                return "Division"

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self.data[index.row()][index.column()]
        else:
            return QVariant()
    
    def sort(self, column, order):
        if self.data:
            self.layoutAboutToBeChanged.emit()
            self.data.sort(key=lambda x: x[column].lower(), reverse=(order == Qt.DescendingOrder))
            self.layoutChanged.emit()
        
    
class TournamentInfoModel(QAbstractTableModel):
    """
    Extends QAbstractTableModel to display information about PPBs. The
    corresponding view is on the right pane of the main window.
    """
    def __init__(self, name=None, division=None):
        super().__init__()

        self.init_data(name, division)

    def init_data(self, name, division):
        """
        Populates the model with team names and their corresponding PPBs
        If given a null tournament name, silently populates an empty model.
        """
        if not name:
            self.result_data = None
            self.tourney_data = None
            self.tourney_id = None
            return

        cursor.execute("SELECT *"
                       " FROM tourneys AS t"
                       " WHERE t.name = '{}' AND t.division = '{}' LIMIT 1".format(name, division))

        self.tourney_data = dict(zip(['id', 'name', 'date', 'qset', 'source', 'division', 'url', 'status'],
                                     cursor.fetchone()))
        self.tourney_data['date'] = self.tourney_data['date'].strftime("%Y-%m-%d")

        cursor.execute("SELECT r.name, naqt_name, ppb"
                       " FROM results AS r"
                       " JOIN tourneys AS t"
                       " ON r.tourney_id = t.id"
                       " WHERE t.name = '{}' AND t.division = '{}'"
                       " ORDER BY r.name".format(name, division))

        tmp_result_data = [list(row) for row in cursor]
        for row in tmp_result_data:
            row[2] = str(row[2])
        
        if self.result_data:
            old_count = len(self.result_data)
        else:
            old_count = 0
            
        new_count = len(tmp_result_data)
        row_difference = new_count - old_count
        
        if row_difference > 0:
            self.beginInsertRows(QModelIndex(), old_count, new_count)
            self.result_data = tmp_result_data
            self.endInsertRows()
        elif row_difference < 0:
            self.beginRemoveRows(QModelIndex(), new_count, old_count)
            self.result_data = tmp_result_data
            self.endRemoveRows()
        else:
            self.result_data = tmp_result_data
            
        self.sort(2, Qt.DescendingOrder)

    def write_new_data(self):
        """
        Function which updates the database with the current data.
        """

        if self.tourney_data:
            values = list(self.tourney_data.values())
            
            query = "UPDATE tourneys SET" + \
                    " name='{}', tourney_date='{}', qset='{}', source='{}', division='{}', url='{}', status='{}' " + \
                    " WHERE id={};"
            cursor.execute(query.format(*values[1::], values[0]))

            insert_id = cursor.lastrowid

            for result in self.result_data:
                query = "UPDATE results SET" + \
                        " name='{}', naqt_name='{}', ppb={}"+ \
                        " WHERE naqt_name='{}' AND tourney_id={};"
                
                cursor.execute(query.format(*result, result[1], insert_id))
            
            connection.commit()

    def removeRow(self, row, parent):
        self.beginRemoveRows(QModelIndex(), len(self.result_data), len(self.result_data) - 1)
        del self.result_data[row]
        self.endRemoveRows()

    def flags(self, index):
        """
        Table cells should be selectable, editable, and enabled.
        """
        return Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsEnabled

    def columnCount(self, parent):
        """
        Returns 3: columns for team name, NAQT team code, and PPB.
        """
        return 3

    def rowCount(self, parent):
        if not self.result_data or parent.isValid():
            return 0
        else:
            return len(self.result_data)

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole and orientation == Qt.PortraitOrientation:
            if section == 0:
                return "Name"
            elif section == 1:
                return "Code"
            elif section == 2:
                return "PPB"
        return None

    def setData(self, index, value, role):
        """
        Updates the internal representation of the tourney_data as according to user
        input. Does not change the database; call write_new_data to do so.
        """
        self.last_changed_value = value
        if role == Qt.EditRole:
            # error checking for editing the PPB column to be nondecimal or non-correctly-ranged
            if index.column() != 2 or (re.match("[0-9]{1,2}\.[0-9]{2}", value) and 0 <= float(value) <= 30):
                self.result_data[index.row()][index.column()] = value
        return True

    def data(self, index, role):
        if role == Qt.DisplayRole and self.result_data:
            return self.result_data[index.row()][index.column()]
        return QVariant()
    
    def sort(self, column, order):
        if self.result_data:
            
            def sort_order(x):
                if column != 2:
                    return x[column]
                else:
                    return float(x[column])
                
            self.layoutAboutToBeChanged.emit()
            self.result_data.sort(key=sort_order, reverse=(order == Qt.DescendingOrder))
            self.layoutChanged.emit()
            
            
class SetListModel(QAbstractTableModel):
    
    # TODO: make this auto-update
    
    def __init__(self):
        super().__init__()
        
        self.set_data()
        
    def set_data(self):
        query = "SELECT qset, count(qset) FROM tourneys GROUP BY qset"
        cursor.execute(query)
        
        self.data = [line for line in cursor]
        
    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole and orientation == Qt.PortraitOrientation:
            if section == 0:
                return "Set"
            elif section == 1:
                return "Count"
        
    def data(self, index, role):
        if self.data and role == Qt.DisplayRole:
            return self.data[index.row()][index.column()]
        
    def sort(self, column, order):
        if self.data:
            
            def sort_order(row):
                if column == 0:
                    return row[column]
                else:
                    return float(row[column])
                
            self.layoutAboutToBeChanged.emit()
            self.data.sort(key=sort_order, reverse=(order == Qt.DescendingOrder))
            self.layoutChanged.emit()
    
    def rowCount(self, parent):
        return len(self.data)
    
    def columnCount(self, parent):
        return 2
    
    def flags(self, index):
        return Qt.ItemIsEnabled
    
    
class TeamListModel(QAbstractTableModel):
    def __init__(self):
        super().__init__()
        self.set_data()
        
    def set_data(self):
        query = "SELECT name, naqt_name, COUNT(naqt_name) FROM results GROUP BY naqt_name"
        cursor.execute(query)
        
        self.data = [line for line in cursor]
        
        self.sort(2, Qt.DescendingOrder)

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole and orientation == Qt.PortraitOrientation:
            if section == 0:
                return "Name"
            elif section == 1:
                return "NAQT Name"
            elif section == 2:
                return "Count"

    def data(self, index, role):
        if self.data and role == Qt.DisplayRole:
            return self.data[index.row()][index.column()]

    def sort(self, column, order):
        if self.data:
            self.layoutAboutToBeChanged.emit()
            self.data.sort(key=lambda x: x[column], reverse=(order == Qt.DescendingOrder))
            self.layoutChanged.emit()

    def rowCount(self, parent):
        return len(self.data)

    def columnCount(self, parent):
        return 3

    def flags(self, index):
        return Qt.ItemIsEnabled
 