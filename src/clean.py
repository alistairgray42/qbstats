#! venv/bin/python
"""
Contains some functions to clean the data in the SQL database
"""
import re
import mysql.connector as mariadb

connection = mariadb.connect(user='qbstats', database='qbstats')
cursor = connection.cursor()


def generate_naqt_aliases():
    """
    Uses the NAQT results in the results table to write some aliases to the aliases table.
    Does not write exact matches.
    """
    
    cursor.execute("SELECT r.name, r.naqt_name FROM results AS r JOIN tourneys as t"
                   " ON r.tourney_id = t.id WHERE t.source='NAQT'"
                   " GROUP BY left(r.naqt_name, length(r.naqt_name) - 1)")
    for row in cursor.fetchall():
        name = row[0]
        naqt_name = row[1][0:-1]
        if re.match("^.+ [A-Z]$", name):
            name = name[0:-2]
        
        query = "INSERT IGNORE INTO aliases (name, naqt_name) VALUES ('{}', '{}')".format(name, naqt_name)
        cursor.execute(query)
        
    connection.commit()


generate_naqt_aliases()
