#! venv/bin/python

"""
Provides methods to turn links into stats tourney_data, using the methods in parser.py.

"""

import scraper
import urllib.request
from bs4 import BeautifulSoup


def hsqb_many(url):
    """
    Given a URL to an HSQB list of stats, returns links to each of the stats pages.
    Links returned are not to the actual stat reports, but to the landing page
    with tournament information and links to each report.
    """
    content = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(content, 'html.parser')
    
    ret = []
    
    for tourney in soup.findAll(attrs={"class":"Name"}):
        stats_url = 'http://www.hsquizbowl.org/db/' + tourney.a['href']
#         middle_soup = BeautifulSoup(urllib.request.urlopen(stats_url).read(), 'html.parser')
        ret.append(stats_url)

#         if middle_soup.find(id="Stats"):
#             statsurl = 'http://www.hsquizbowl.org/db/'
#             statsurl += middle_soup.find(id="Stats").findAll("li")[-1].a['href']
#             #Always takes the last link in the stats section, which is far from perfect
#             ret.append(statsurl)
            
    return ret


def hsqb_one(url):
    """
    Given a URL to an SQBS stat report, returns a dictionary containing the
    tournament name, date, URL, source ("HSQB"), and a default status value
    ("Unchecked"), as well as a dummy list of known sets used (containing
    only "Unknown") and a dictionary from set names (only "Unknown") to
    dictionaries of team names to PPBs.
    """
    data = scraper.hsqb_parse(url)
    data["url"] = url
    data["source"] = "HSQB"
    data["status"] = "Unchecked"
    data["division"] = "None"
    
    return data


def naqt_count(url):
    """
    Given a URL to a NAQT list of stats, returns the number of non-*NCT
    tournaments with "Complete" stats listed.
    """
    count = 0
    
    soup = BeautifulSoup(urllib.request.urlopen(url).read(), 'html.parser')
    for row in soup.findAll("tr"):
        cells = row.findAll(["td", "th"])
        if cells[3].text == "Complete":
            if "National Championship" in cells[1].text:
                continue
            count += 1
            
    return count


def naqt_many(url):
    """
    Given a URL to a NAQT list of stats, returns the URLs of non-*NCT
    tournaments with "Complete" stats. (Does not guarantee that those
    stats are complete or usable)
    """
    soup = BeautifulSoup(urllib.request.urlopen(url).read(), 'html.parser')
    ret = []
    
    for row in soup.findAll("tr"):
        cells = row.findAll(["td", "th"])
        if cells[3].text == "Complete":
            url = "https://www.naqt.com" + cells[1].a.get('href')
             
            if "National Championship" in cells[1].text:
                continue
            ret.append(url)
            
    return ret
