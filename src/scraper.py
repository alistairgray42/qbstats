#! venv/bin/python

"""
Scrapes and parses the content of a stats page, writing to the database.

For each tournament, writes the following fields:
    id : automatically-generated tournament id (primary key)
    name : name of tournament
    tourney_date : date of tournament
    division : tournament division
    qset : question set used
    source : "NAQT" or "HSQB", as appropriate
    url : URL where stats are found
    status : initially set to "Unchecked", to denote a tournament whose results haven't been manually verified

For each result (one team in one tournament), writes the following:
    name : the name of the team whose result this is, exactly as on the stats page (composite primary key)
    tourney_id : the id of the tournament (composite primary key)
    ppb : a Decimal object containing the ppb

Can handle both NAQT and HSQB stats pages. (or should, at least)
"""

import urllib.request
import datetime
import re
from bs4 import BeautifulSoup
import mysql.connector as mariadb

connection = mariadb.connect(user='qbstats', database='qbstats')
cursor = connection.cursor()


def naqt_parse(url):
    """
    Given the URL of an NAQT stats page, write the PPBs from that tournament to the database.
    """

    content = urllib.request.urlopen(url)
    soup = BeautifulSoup(content, 'html.parser')

    tourney_name = soup.h1.text
    date = soup.th.text

    # this is not a hyphen
    if "–" in date:
        date = date[date.index("–") + 2:]
    date = str(datetime.datetime.strptime(date, "%A, %B %d, %Y").date())
    
    # Check that we don't have duplicate tournaments
    query = 'select count(*) from tourneys where name="{}" and tourney_date="{}";'.format(tourney_name, date)
    cursor.execute(query)
    
    if cursor.fetchone()[0] > 0:
        return

    stuff = __naqt_parse_interior(soup)

    if not stuff:
        print("Skipped {}!".format(tourney_name))
        return

    qset, names, schools, ppbs, divisions = stuff

    insert_ids = dict()
    # insert one 'tournament' per division returned

    for division in set(divisions):
        sql_tourneys_values = '("{0}","{1}","{2}","NAQT","{3}","{4}","Unchecked")'\
            .format(tourney_name, date, qset, division, url)

        cursor.execute("insert ignore into tourneys (name, tourney_date, qset, source, division, url, status) values {}"
                       .format(sql_tourneys_values))

        insert_ids[division] = cursor.lastrowid

    for i in range(len(names)):
        sql_values = '("{0}","{1}",{2},{3})'.format(names[i], schools[i], insert_ids[divisions[i]], ppbs[i])
        cursor.execute("replace into results (name,naqt_name,tourney_id,ppb) values {}".format(sql_values))

    connection.commit()


def __naqt_parse_interior(soup):
    """
    Given a BeautifulSoup object of a division's stats (not a full results page),
    return some tournament info
    """
    ppbs = []
    names = []
    schools = []
    divisions = []

    qset = "Unknown"
    division = ""

    # loop through every table row and heading in the body
    for row in soup.findAll(["tr", "h2"]):
        cells = row.findAll(["th", "td"])
        
        # for some reason, this length is 5 for the two-column tables up top
        is_body_table = len(cells) > 5

        # assign the question set if not already assigned
        if not is_body_table and row.td is not None and row.td.string == "Packet Set:":
            qset = row.th.string
            continue
            
        # we do all this processing of the division name each loop because we don't know when we'll hit a new division
            
        # assign the division name if not already assigned
        if row.name == "h2":
            division = row.text.strip("\n").strip("\t")
            continue

        if not division:
            division = "None"

        # some tournaments have multiple divisions; this excludes middle school divisions
        if "Middle School" in division or "MS" in division:
            continue

        # when of the form "Division1 (IS-42)"
        if "(" in division:
            qset = division[division.find("(") + 1: division.rfind(")")]
            division = division[:division.find("(") - 1]
        
        if "Division" in division:
            division = division[:division.find("Division") - 1]

        is_table_header = row.findAll("th")[-1].has_attr("title")

        # trying to robustly find the PPB column in the table, which may differ between rulesets
        if is_body_table and is_table_header:
            for i in range(len(cells)):
                text = cells[i].string.strip("\n").strip("\t").strip(" ")

                if text == "PPB":
                    ppb_position = i
                    break
                    
            # if there is no ppb, error out
            if 'ppb_position' not in locals():
                return

        # main body of the logic: runs once per team
        if is_body_table and not is_table_header:
            
            # the human-readable name of the team
            name = row.a.text.replace('"', '').replace("'", '')
            
            # NAQT's team id
            team_id = row.a['href']
            team_id = team_id[team_id.rfind("=") + 1:]
            school_id = ''

            if name == 'Bye' or name == 'Spoiler':
                continue

            # ooooof at having to do this; very expensive but necessary(?) step to get school_id from team_id
            team_soup = BeautifulSoup(urllib.request.urlopen(
                "https://www.naqt.com/stats/tournament/team.jsp?team_id={}".format(str(team_id))), 'html.parser'
            )

            team_soup = team_soup.find("section", {"id": "tournament-info"})
            for row in team_soup.findAll("tr"):
                if row.td.text == "School:":
                    school_id = row.th.a['href']
                    school_id = school_id[school_id.rfind("=") + 1:]

                    # teams without identifier letters are A teams
                    if re.match(".+ [A-Z]$", name):
                        school_id += name[-1]
                    else:
                        school_id += "A"
                    break

            ppb = cells[ppb_position].text
            if ppb == " ":
                # nonbreaking space, when there's a forfeit game
                continue

            ppb = float(ppb)

            ppbs.append(ppb)
            names.append(name)
            schools.append(school_id)
            divisions.append(division)

    # strips last two characters off the end (idk when this happens??)
    if "(" in qset:
        qset = qset[:qset.rfind("(") - 1]

    return qset, names, schools, ppbs, divisions


def hsqb_parse(url):
    """
    Given a BeautifulSoup object parsed from an SQBS stats report page, return
    a dictionary containing tournament name, date, a dummy list of sets
    containing only "Unknown", and a ppbs dictionary from "Unknown" to
    a dictionary of team names to PPBs.
    """

    content = urllib.request.urlopen(url)
    soup = BeautifulSoup(content, 'html.parser')

    header = soup.find(class_="Tournament").text
    if ' on ' in header:
        (tourney_name, date) = header.split(' on ')
        date = str(datetime.datetime.strptime(date, "%B %d, %Y").date())
    elif ' - ' in header:
        (tourney_name, date) = header.split(' from ')
        date = date.split(' - ')[1]
        date = str(datetime.datetime.strptime(date, "%B %d, %Y").date())
    else:
        # ugly hack for multiple dates
        (tourney_name, date) = header.split(' from ')
        date = date[:date.index(" ") + 1] \
               + date[date.index(" ") + 4:]
        date = str(datetime.datetime.strptime(date, "%B %d, %Y").date())

    ret = dict()

    qset = "Unknown"

    ppb_position = -1

    for row in soup.find_all("tr"):
        if "P/B" in row.text:
            contents = [i for i in row.contents if not i == "\n"]
            for i in range(len(contents)):
                if contents[i].text == "P/B":
                    ppb_position = i
                    break
        if row.td.text not in ("Rank", "Standings", "Team") and not row.td.text.isspace():
            name = row.a.text.replace(".", "")
            ppb = float(row.findAll("td")[ppb_position].text)
            ret[name] = ppb

    return {"name": tourney_name, "date": date, "sets": [qset], "ppbs": {qset: ret}}
