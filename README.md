QBStats
=========

## About
This is a project whose purpose is to rank high school quizbowl teams automagically.

## Notes to myself so I don't go crazy
import_stats.py writes stats to the mongoDB database, derived from raw information scraped from NAQT and HSQB stats pages that is then parsed by the functions in parser.py.

Because those data are freshly-scraped, they have to be cleaned; that's the function of ui.py. The GUI in that file is used to view and edit data, as well as calculate stats. 

## TODOs
* Programmatically eliminate duplicate tournaments
* Come up with a way to save changes to reproduce databases
* Write ranker...
* Conform to PEP-8 lol
* HTML frontend (investigate frameworks)

## Things that aren't necessarily to do but that i should consider doing
* Smoosh import_stats and parser together